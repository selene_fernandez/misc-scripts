#!/usr/bin/env perl
#
## Code by: Selene L. Fernandez Valverde 
## July 2015
## Script that renames all strings matching a file of old and new names
#
## Assummes all files are in the same folder

#####################################
##
## The MIT License
##
## Copyright (c) 2015 Selene L. Fernandez-Valverde
##
## Permission is hereby granted, free of charge, to any person obtaining a copy
## of this software and associated documentation files (the "Software"), to deal
## in the Software without restriction, including without limitation the rights
## to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
## copies of the Software, and to permit persons to whom the Software is
## furnished to do so, subject to the following conditions:
##
## The above copyright notice and this permission notice shall be included in
## all copies or substantial portions of the Software.
##
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
## OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
## THE SOFTWARE.
##
######################################
#
use warnings;
#use strict;
use Data::Dumper;
use Getopt::Long qw( :config bundling );

#my () = ();

$script = ( split"/", $0 )[ -1 ];

$usage = qq(
$script by Selene L. Fernandez-Valverde, July 2015.

$script renames strings in a file using a tab delimited file as table. 
Regular order renames strings in the first column in the tabular file to 
that in column 2 or viceversa if reverse switch is on. If names are split
in two different lines they will not be renamed. 
Useful for renaming newick trees for Phylip or other phylogenetic programs
with limitations in the number of characters. 

NOTE: Avoid non standard characters to reduce chances of script failure. 

Usage: $script [options] -i <Tab delimited file> -f <File to rename> -o <Renamed file>

Options:
	[--table     | -t ] - Input renamer tab file (column1 = original name, column2 = new name)
	[--file      | -f ] - Text file to rename.
	[--output    | -o ] - Desired output file file
	[--reverse   | -r ] - If added will reverse the order of replacement from new name (second column) to old name (first column). Turned off by default.

Example:

$script -i Renaming_table.txt -f Dollop_summary.txt -o Renamed_Dollop_Summary.txt

);

GetOptions(
	\%options,
	"table|t=s",
	"output|o=s",
	"file|f=s",
	"reverse|r",
	);

# Reading variables
$table = $options{ "table" };
$output = $options{ "output" };
$file = $options{ "file" };

print STDERR "\nERROR: No renamer tab file provided\n $usage" and exit if not $table ;
print STDERR "\nERROR: No output file given. Please provide an output file\n$usage" and exit if not $output;
print STDERR "\nERROR: No file to be renamed provided. \n$usage" and exit if not $file;


################################################### MAIN ################################################################


open ($table_hd, "$table")|| die "Cannot open tabular rename file\n";

while(<$table_hd>){
	$line = $_;
	chomp($line);
	if ($options{ "reverse"}){
		($original,$new)=(split/\t/,$line);
		$renamer{$new}=$original;
	}
	else{
		($original,$new)=(split/\t/,$line);
		$renamer{$original}=$new;
	}
}
close($table_hd);

#print Dumper (%renamer);

open ($file_hd, "$file") || die "Cannot open file to rename\n";
open ($output_hd, ">$output") || die "Cannot create output file provided\n";

while(<$file_hd>){
	$line = $_;
	chomp($line);
	foreach $entry (keys(%renamer)){
		$line=~s/$entry/$renamer{$entry}/;
	}
	print $output_hd "$line\n";
}

close($table_hd);
close($output_hd);
close($file_hd);

