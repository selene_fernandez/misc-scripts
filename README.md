This repository cointains scripts for parsing, converting and/or extracting data from a range of formats. 

I try to only add on working scripts but use at your own risk. 

Script list: 

* fastortho2fasta.pl - Gets fasta for orthology groups inferred using FastOrtho
* FastOrtho2CAFE.pl - Converts orthology groups inferred using FastOrtho to suitable output for CAFE
* fastortho2dollop.pl - Converts orthology groups inferred using FastOrtho to suitable output for dollop (Phylip)
* wrap_dollop.pl - Wrapper for dollop (Phylip) NOTE: harcoded options inside
* parse_dollop.pl - Parses dollop output and generates a summary table with character gains and losses 
* rename_within_file.pl - Renames a text file using a tabular file having a two column list with the original and replacement name

Citations: 

If you make use of any of these scripts please cite:

https://bitbucket.org/selene_fernandez/misc-scripts 



