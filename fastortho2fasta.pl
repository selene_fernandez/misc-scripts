#!/usr/bin/env perl

# Code by: Selene L. Fernandez Valverde 
# April 2015
# Script that gets fastas from FastOrtho groups

# Assummes all files are in the same folder

#####################################
#
# The MIT License
#
# Copyright (c) 2015 Selene L. Fernandez-Valverde
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
####################################

use warnings;
use strict;
use Data::Dumper;

my ($fastortho_hd, $line, $group_number, $genes, $group_name, 
   @genes, $gene, $gene_name, $gene_file, %files, @temp_file_genes, 
   $temp_hd, $cmd, $printer, $gene_file_2) = ();

open ($fastortho_hd, "$ARGV[0]") || die "No FastOrtho group file provided\n";

`mkdir FastOrtho_FASTAS`;

while(<$fastortho_hd>){
	$line = $_;
	chomp($line);
	($group_number,$genes) = (split/\t/,$line);
	$group_name=(split / /,$group_number)[0];
	@genes = (split / /,$genes);
	shift(@genes); # Removing empty value
#	print Dumper (@genes);
	foreach $gene (@genes){
#		print $gene; <STDIN>;
		$gene =~ m/(.+)\((.+)$/;
		$gene_name=$1;
		$gene_file=$2;
		$gene_file=~ s/\)//;
		###($gene_name,$gene_file) = (split /\(/,$gene);
#		print "Gene $gene_name $gene_file\n"; 
		$gene_file.=".fasta";
		push (@{$files{$group_name}{$gene_file}},$gene_name);
	}

	foreach $gene_file_2 (keys %{ $files{$group_name} }){
		@temp_file_genes = @{$files{$group_name}{$gene_file_2}};
		$printer = join("\n",@temp_file_genes);
		open( $temp_hd, ">/tmp/temp.txt");
		print $temp_hd "$printer";
		$cmd ="faSomeRecords $gene_file_2 /tmp/temp.txt /tmp/temp.fa";
		`$cmd`;
		`cat /tmp/temp.fa >> ./FastOrtho_FASTAS/$group_name\_sequences.txt`;
		`rm /tmp/temp.fa /tmp/temp.txt`;
	}
}

#print Dumper (%files);

#ORTHOMCL7615 (2 genes,1 taxa):	 evm.model.scaffold65.5(EVM2_Models_pasa_oki_pep) evm.model.scaffold7.233(EVM2_Models_pasa_oki_pepl).

# Performance
#real	10m6.790s
#user	7m14.089s
#sys	2m52.355s
#7625 orthogroups

