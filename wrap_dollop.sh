#!/bin/sh

function usage() {

echo " wrap_dollop.sh by Selene Fernandez, July 2015."
echo "" 
echo " wrap_dollop.sh " 
echo " Runs dollop (Phylip) in a user friendly manner. Assumes phylip is in path." 
echo ""
echo " Usage: wrap_dollop.sh -c <Classification> -G <Genome bedfile> -m <Gene models (bedfile)> -g <gaps (bedfile)>"
echo "" 
echo " Example: "
echo " wrap_dollop.pl  "
echo "" 
echo " Options:"
echo " [ -i ] - dollop input file (can be generated using fastortho2dollop.pl)"
echo " [ -t ] - input tree - if no trees i provided dollop will try to find the best parsimony tree. "
echo " [ -o ] - output file in user friendly format"
echo "" 
exit 1
}

if [ -z "$1" ]
  then
      echo -e "\nERROR: Please supply arguments\n"
      usage
fi

while getopts "i:t:o:" opt 
do 
    case "$opt" in
  	i) input="$OPTARG" ;;
  	t) tree="$OPTARG" ;;
  	o) output="$OPTARG" ;;
  	?) echo "Invalid option; -$OPTARG "; usage ; exit >&2 ;;
    esac
done

if [ ! -f ${input} ];
then
	echo -e "\nERROR: File ${input} not found!\n"
	usage
fi

if [ ! -f ${tree} ];
then
	echo -e "\nERROR: File ${tree} not found!\n"
	usage
fi


echo -e "\nRunning dollop\n\n"
cp -i ${input} infile
if [[ -f ${tree} ]] # Testing if variable is empty
then
	echo -e "RUN: dollop with user tree\n"
	cp -i ${tree} intree
	dollop << END1
U
1
4
5
Y
END1
	rm intree
else
	echo -e "RUN: dollop will find best tree\n"
	dollop << END2
1
4
5
Y
END2
fi

mv outfile ${output}
mv outtree ${output}.tree
rm infile
