#!/usr/bin/env perl	

# Script takes minimum and maximum of a table with repeated entries
#
#use strict;
use Getopt::Long qw( :config bundling);
use Data::Dumper;
use List::Util qw( min max );

use constant {
    SEQ_NAME =>0,
    HSTART =>1,
    HEND =>2,
};

my ($script, $usage, %options, $files, $field);

$script = ( split"/", $0 )[ -1 ];

$usage= qq(
$script by Selene Fernandez-Valverde, November 2015.
BETA

$script collapses repeated entries and prints out the max and min of the hit

Usage: $script [options] < file(s)>*

Options : 
        [--input  | -i <int>  ]   - Input file to collapse
        [--output | -o <int>  ]   - Collapsed output file 
);

GetOptions(
	\%options,
    "input|i=s",
    "output|o=s",
);

$input = $options{ "input" };
$output = $options{ "output" };

print STDERR $usage and exit if not $input;
print STDERR $usage and exit if not $output;


################################################### MAIN ##################################################

my ($input_hd, $output_hd) =();

open ($input_hd ,"$input");
open ($output_hd ,">$output");


while (<$input_hd>){
    chomp ($_);
    $line = $_;
    ($seq_name, $hstart, $hend) = ( split /\s/,$line); 
    push(@{$start{$seq_name}},$hstart);
    push(@{$end{$seq_name}},$hend);
}

#print Dumper (%start);
#print Dumper (%end);


foreach $key (keys(%start)){
	$min_start=min(@{$start{$key}});
	$max_end=max(@{$end{$key}});
	print $output_hd "$key\t$min_start\t$max_end\n";
	}

close($input_hd);
close($output_hd);


