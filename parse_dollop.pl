#!/usr/bin/env perl
#
## Code by: Selene L. Fernandez Valverde 
## July 2015
## Script that parses Dollop output and generates a summary table of gains and losses
#
## Assummes all files are in the same folder

#####################################
##
## The MIT License
##
## Copyright (c) 2015 Selene L. Fernandez-Valverde
##
## Permission is hereby granted, free of charge, to any person obtaining a copy
## of this software and associated documentation files (the "Software"), to deal
## in the Software without restriction, including without limitation the rights
## to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
## copies of the Software, and to permit persons to whom the Software is
## furnished to do so, subject to the following conditions:
##
## The above copyright notice and this permission notice shall be included in
## all copies or substantial portions of the Software.
##
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
## OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
## THE SOFTWARE.
##
######################################
#
use warnings;
use strict;
use Data::Dumper;
use Getopt::Long qw( :config bundling );

my ($usage, %options, $dollop, $output, $line_numb, $steps, $steps_hd,
    $output_hd, $line, $transitions, $zeros, $ones, $points, $from, $to,
    $aux, $vector, $script, $line_numb_end, $tree, $tree_lines) = ();

$script = ( split"/", $0 )[ -1 ];

$usage = qq(
$script by Selene L. Fernandez-Valverde, July 2015.

$script counts number of gains and losses  (from Phylip) input compliant Phylip file.
The output cointains a table of gains and losses at each node as well as the tree used
for the transition calculation.

Usage: $script [options] -d <Dollop output> -o <Output file >

Options:
	[--dollop      | -d ] - Dollop output file
	[--output      | -o ] - Desired output file 

Example:

$script -d Dollop_output.txt -o GainLoss_Summary.txt

);

GetOptions(
	\%options,
	"output|o=s",
	"dollop|d=s",
	);

# Reading variables
$output = $options{ "output" };
$dollop = $options{ "dollop" };

print STDERR "\nERROR: No output file given. Please provide an output file\n$usage" and exit if not $output;
print STDERR "\nERROR: No file given with original fasta files provided. Please provide.\n$usage" and exit if not $dollop;


################################################### MAIN ################################################################
$line_numb = `grep -n From $dollop | cut -f 1 -d ':'`;
$line_numb+=3;

$steps = `tail -n+$line_numb $dollop`;
#print "$steps";

open $steps_hd, "<", \$steps;
open ($output_hd, ">$output");
print $output_hd "From\tTo\tUnchanged\tGains\tLosses\n"; 

# making separator null
$transitions = "";

while(<$steps_hd>){
	$line = $_;
	chomp($line);
	if ($line=~ /^\s{27}/){
		$line =~ s/ //g;
		#print "Line $line\n";
		$transitions.=$line; 
	}
	else{
		#### Processing previous entry before deleting
		if ($transitions ne ""){
			#print $output_hd "$transitions\n"; 
			$zeros = $transitions =~ tr/0/0/;	
			$ones = $transitions =~ tr/1/1/;	
			$points = $transitions =~ tr/\./\./;	
			print $output_hd "$from\t$to\t$points\t$ones\t$zeros\n"; 
			$transitions = "";
		}
		if ($line =~ /^$/){
			next;
		}
		#### Processing new entry
		$line =~ s/^ +//;
		$line =~ s/ +/\t/;
		$line =~ s/ +/\t/;
		$line =~ s/ +/\t/;
		($from,$to,$aux,$vector)=(split /\t/,$line); 
		$vector =~ s/ //g;
		#print "First Line $from\t$to\t$aux\t$vector\n";
		$transitions=$vector;
	}
}

close($steps_hd);

$line_numb = `grep -n tree\: $dollop | cut -f 1 -d ':'`;
chomp($line_numb);
$line_numb_end = `grep -n requires $dollop | cut -f 1 -d ':'`;
$tree_lines = $line_numb_end-$line_numb;
$tree = `tail -n+$line_numb $dollop | head -n $tree_lines`;

print $output_hd "\n\n$tree";

close($output_hd);

