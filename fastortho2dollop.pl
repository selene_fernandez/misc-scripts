#!/usr/bin/env perl
#
## Code by: Selene L. Fernandez Valverde 
## July 2015
## Script that generates Dollop (from Phylip) input files from FastOrtho files
#
## Assummes all files are in the same folder

#####################################
##
## The MIT License
##
## Copyright (c) 2015 Selene L. Fernandez-Valverde
##
## Permission is hereby granted, free of charge, to any person obtaining a copy
## of this software and associated documentation files (the "Software"), to deal
## in the Software without restriction, including without limitation the rights
## to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
## copies of the Software, and to permit persons to whom the Software is
## furnished to do so, subject to the following conditions:
##
## The above copyright notice and this permission notice shall be included in
## all copies or substantial portions of the Software.
##
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
## OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
## THE SOFTWARE.
##
######################################
#
use warnings;
use strict;
use Data::Dumper;
use Getopt::Long qw( :config bundling );

my ($fastortho_hd, $line, $group_number, $genes, $group_name, 
    @genes, $gene, $gene_name, $gene_file, %files, @temp_file_genes, 
    $temp_hd, $cmd, $printer, $gene_file_2, $organism, $gene_bundle,
    %counter, $script, $usage, %options, $input, $output, $group,
    $org, $dollop_hd, $fastas, $fastas_hd, @Orgs, $species, $characters,
    $sub, $trimmed_name, $species_hd) = ();

$script = ( split"/", $0 )[ -1 ];

$usage = qq(
$script by Selene L. Fernandez-Valverde, July 2015.

$script converts a FastOrtho groups output into a Dollop (from Phylip) input compliant Phylip file.
Because of the specs of the phylip file the first 5 and last five characters of each file are used as ids. You can check the ids are unique by revising the SpeciesIDS_<output> file. 

Dollop (from Phylip) can be found here - http://evolution.genetics.washington.edu/phylip/doc/dollop.html

Usage: $script [options] -i <FastOrtho output> -f <Text file with list of fasta files used for FastOrtho> -o <Dollop (from Phylip) input file>

Options:
	[--input       | -i ] - Input FastOrtho file 
	[--fastas      | -f ] - Text file with list of fasta files used for FastOrtho. One filename per line without tabs (\t).
				Only the suffix before the first point (.) will be used - this must be unique for each file. 
	[--output      | -o ] - Desired output file 

Example:

$script -i Fastortho_input.txt -f Originals_Fasta_File_List.txt -o Dollop_input.txt

);

GetOptions(
	\%options,
	"input|i=s",
	"output|o=s",
	"fastas|f=s",
	);

# Reading variables
$input = $options{ "input" };
$output = $options{ "output" };
$fastas = $options{ "fastas" };

print STDERR "\nERROR: No fastortho input provided, please provide a fastortho input\n $usage" and exit if not $input ;
print STDERR "\nERROR: No output file given. Please provide an output file\n$usage" and exit if not $output;
print STDERR "\nERROR: No file given with original fasta files provided. Please provide.\n$usage" and exit if not $fastas;


################################################### MAIN ################################################################

# Generating my @Orgs array

open ($fastas_hd, "$fastas");

while(<$fastas_hd>){
	$line = $_;
	chomp($line);
	$org = (split/\./,$line)[0];
	push(@Orgs,$org);
}
close($fastas_hd);

open ($fastortho_hd, "$input") || die "No FastOrtho group file provided\n";

while(<$fastortho_hd>){
	$line = $_;
	chomp($line);
	($group_number,$genes) = (split/\t/,$line);
	$group_name=(split / /,$group_number)[0];
	@genes = (split / /,$genes);
	shift(@genes); # Removing empty value
#	print Dumper (@genes);
	foreach $gene_bundle (@genes){
		$gene_bundle=~ s/\)//;
		($gene_name,$organism) = (split /\(/,$gene_bundle);
		$organism = (split/\./,$organism)[0];
		$counter{$group_name}{$organism}++;
	}	
}

#print Dumper (%counter); 

open ($dollop_hd, ">$output") || die "Cannot create output file\n$usage\n";
open ($species_hd, ">SpeciesIDS_$output") || die "Cannot create output file\n$usage\n";

# Sorint hash keys - http://perldoc.perl.org/functions/sort.html

my @sorted_keys = map { $_->[0] }
           sort { 
		   $a->[1] <=> $b->[1]
           } map { [$_, /ORTHOMCL(\d+)/] } (keys %counter);

#print Dumper (@sorted_keys);
$species = scalar(@Orgs);
$characters = scalar(@sorted_keys);

print $dollop_hd "$species $characters\n";
print $species_hd "Species\tPhylipID\n";

foreach $org (@Orgs){
	$sub=substr($org,-9,5); 
	$trimmed_name = sprintf ("%.5s%s\n",$org,$sub); 
	print $dollop_hd "$trimmed_name";
	print $species_hd "$org\t$trimmed_name";
	foreach $group (@sorted_keys){
		if ($counter{$group}{$org}){
			print $dollop_hd 1;
		}
		else{
			print $dollop_hd 0;
		}
	}
	print $dollop_hd "\n";
}

close($dollop_hd);
close($species_hd);
